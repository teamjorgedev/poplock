# poplock

__`poplock`__ is a simple to use plug-and-play webserver written in __GO__.

## Description

The idea is for poplock to be used as a tiny webserver for use on local machines/networks.
`poplock` is not intended to be used as a commercial/external web server.

The motivation for creating poplock was to have a low overhead webserver where
I could easily drop and host documentation/learning/faq html files. The file creation
process should ideally be writing an intended page in `Markdown` and coverting it to HTML.
Fortunately, there are many `MD` -> `HTML` tools to simplify this process and reducing time
spent.

## Getting Started

### Prerequisites

Please ensure you have the latest version of `GO` installed. [Install GOLANG](https://golang.org/doc/install)

poplock only uses one external package/library:

__Gorilla/Mux__

Package gorilla/mux implements a request router and dispatcher for matching incoming requests to their respective handler.

`go get -u github.com/gorilla/mux`

### Compiling `poplock`

* Clone the project onto your local machine.

* In Terminal/CMD, navigate to the root of the project. `/poplock/`.

* Run the following command: `go build`.

### Configuration

* Open the __config.json__ file in your favorite text editor.

* Modify the __PagesDir__ property to contain the relative or absolute path to
  the directory containing your `.html` files

* __For example:__

  *  Absolute path: `/home/user/Documents/poplock/shared/www`

  *  Relative path (From `poplock` executable): `./shared/www`

* Modify the __Port__ property in the __config.json__ file. Default port will be
8081.

  __(_Other properties are not currently being utilized, but might be at a later stage_)__

### Running `poplock`

* Execute the `poplock` (`poplock.exe` for Windows) executable that was created
by the build command.

* You can access it by navigating to `http://localhost:8081/`. The Home page should
display a list of the available files/links

* Web routes should be the same as your folder structure. For example:

  ![](https://i.imgur.com/F7fNt36.png "Web page folder structure")

  * If you would look to view the _customer_ documentation pages, the route would be:

    `http://localhost:8081/documentation/customer/addcustomer`

    __OR__

    `http://localhost:8081/documentation/customer/configurecustomer`

* You can refresh the pages that are loaded into the webserver by using the `/refresh` path.
This prevents having to restart the webserver every time a new page has to be cached. `/refresh` will essentially scan your webpage directory and add a handler for every __new__ file it finds.

## TODO

* Build command-line instructions OR web interface for controlling server

* Add an index page for each sub-directory to show available pages

* Possibly redo `/` page with Angular + Bootstrap
