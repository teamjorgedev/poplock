package main

import (
	"poplock/shared/lib/configutil"
	"github.com/gorilla/mux"
	"poplock/shared/lib/webutil"
	"net/http"
	"log"
	"time"
)

func main() {
	conf := configutil.LoadConfig()			//LOAD config.json FILE
	log.Println("Server Live: Listening on port " + conf.Port)

	for {
		conf = configutil.LoadConfig()			//LOAD config.json FILE
		time.Sleep(time.Second * 2)
		h := mux.NewRouter()

		srv := &http.Server{					//CREATE HTTP SERVER
			Handler:      h,
			Addr:         "0.0.0.0:" + conf.Port,
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		}

		Webutil.AddHandlers(h, conf.PagesDir)


		//Root handlers
		h.HandleFunc("/", func(response http.ResponseWriter, request *http.Request) {
			response.Write([]byte(Webutil.CreateHomePage(h, Webutil.HomePage{conf.HomePage.Title, conf.HomePage.Caption})))
		})

		//Refresh Handler
		h.HandleFunc("/refresh", func(response http.ResponseWriter, request *http.Request) {
			response.Write([]byte("<p>Service Restarted</p>"))
			log.Println("Service Refreshed by " + request.Host)
			srv.Close()
		})

		srv.ListenAndServe()
		srv = nil
		h = nil
		continue
	}
}