package fileutil

import (
	"strings"
)

const htmlExt = "html"

func IsHTML(fileName string) bool {
	/*
		GET FILE EXTENSION BY SPLITTING FILENAME AND GETTING LAST INDEX
		CHECK IF FILE IS HTML
		RETURN TRUE IF HTML FILE TYPE
	*/
	fSplit := strings.Split(fileName, ".")
	if len(fSplit) < 2 {
		return false
	}
	ext := fSplit[len(fSplit) - 1]
	if strings.ToLower(ext) != htmlExt {
		return false
	}
	return true
}