package configutil

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"poplock/shared/lib/webutil"
)

type Configuration struct {
	PagesDir   string `json:"pagesdir"`
	StyleSheet string `json:"stylesheet"`
	HomePage   Webutil.HomePage `json:"homepage"`
	Port       string `json:"port"`
}

func LoadConfig() Configuration {
	raw, err := ioutil.ReadFile("./config.json")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	var c Configuration
	json.Unmarshal(raw, &c)
	return c
}
