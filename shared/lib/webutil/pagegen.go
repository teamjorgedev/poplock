package Webutil

import (
	"github.com/gorilla/mux"
	"poplock/shared/lib/errutil"
	"io/ioutil"
	"strings"
)

const linkElement_Start = `<div class="wrapper">
								<a class="eigth before after" href="`
const linkElement_Close = ` 	</a>
							</div>`

type HomePage struct {
	Title		string		`json:"title"`
	Caption		string		`json:"caption"`
}

func CreateHomePage(r *mux.Router, hPage HomePage) string {
	content, err := ioutil.ReadFile("./index.html")
	errutil.HandleError(err)

	handlers := ""
	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		t, err := route.GetPathTemplate()
		errutil.HandleError(err)
		if t != "/" && t != "/refresh" {
			handlers += linkElement_Start + t + "\">" + strings.Split(t, "/")[len(strings.Split(t, "/")) - 1] + linkElement_Close
		}
		return nil
	})

	page := string(content)
	page = strings.Replace(page, "$$TITLE$$", hPage.Title, 1)
	page = strings.Replace(page, "$$CAPTION$$", hPage.Caption, 1)
	page = strings.Replace(page, "$$BODY$$", handlers, 1)

	return page
}