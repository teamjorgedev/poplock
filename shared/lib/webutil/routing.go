package Webutil

import (
	"github.com/gorilla/mux"
	"os"
	"io/ioutil"
	"strings"
	"net/http"
	"poplock/shared/lib/errutil"
	"path/filepath"
	"poplock/shared/lib/fileutil"
	"log"
)

func AddHandlers(h *mux.Router, wwwPath string) {
	files := GetFiles(wwwPath)

	if len(files) == 0 {
		log.Fatal("No Files Found. Please add files to the WWW folder before restarting")
	}

	for _, file := range files {
		content, err := ioutil.ReadFile(file.TruePath)
		errutil.HandleError(err)
		h.HandleFunc(file.HandlePath, func(response http.ResponseWriter, request *http.Request) {
			response.Write(content)
		})
	}
}

type WWWFile struct {
	TruePath string
	HandlePath string
}

func GetFiles(wwwPath string) []WWWFile {
	handlerArr := make([]WWWFile, countFiles(wwwPath)) 

	counter := 0
	err := filepath.Walk(	wwwPath,
							func(path string, info os.FileInfo, err error) error {
								errutil.HandleError(err)
								if !info.IsDir() && fileutil.IsHTML(info.Name()) {
									handlerPath := strings.Replace(path, wwwPath, "", 1) 			//REMOVE THE WWW DIRECTORY PORTION FROM PATH
									handlerPath = strings.Replace(handlerPath, "\\", "/", 10)	//REMOVE WINDOWS SEPARATORS AND REPLACE WITH /
									handlerPath = handlerPath[:len(handlerPath) - 5] 						//TAKE OUT .HTML FILE EXTENSION - CREATES A PROPER HANDLER
									if string([]byte(handlerPath)[0]) != "/" {								//IF HANDLER PATH DOES NOT START WITH "/", ADD "/" TO START
										handlerPath = "/" + handlerPath
									}
									handlerArr[counter] = WWWFile{path, handlerPath}		//ADD TO THE HANDLER ARRAY
									counter++
								}
								return nil
	})
	errutil.HandleError(err)
	return handlerArr
	}

func countFiles (wwwPath string) int {
	/*
		WALK THE DIRECTORY AND RETURN THE AMOUNT OF HTML FILES.
	*/
	count := 0
	err := filepath.Walk(	wwwPath,
		func(path string, info os.FileInfo, err error) error {
			errutil.HandleError(err)
			if !info.IsDir() &&  fileutil.IsHTML(info.Name()){
				count++
			}
			return nil
		})
	errutil.HandleError(err)
	return count
}

